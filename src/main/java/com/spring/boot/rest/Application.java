package com.spring.boot.rest;

import org.springframework.web.client.RestTemplate;

public class Application {

	public static void main(String args[]) {
		RestTemplate restTemplate = new RestTemplate();
		Customer customer = restTemplate.getForObject("https://myrestservicep1941345437trial.hanatrial.ondemand.com/myrestservice-0.0.1-SNAPSHOT/customer/get/6",
						Customer.class);

		System.out.println("Name:    " + customer.getFname());
		System.out.println("Surname:    " + customer.getLname());
		System.out.println("Address:    " + customer.getAddress());

	}

}